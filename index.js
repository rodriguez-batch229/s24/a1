// Exponent Operator
let getCube = function (num) {
    return num ** 3;
};

// Template Literals
let givenNum = Math.round(Math.random() * 10);
resultCube = getCube(givenNum);
console.log(`The cube of ${givenNum} is ${resultCube}`);

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

let [houseNumber, street, city, areaCode] = address;
console.log(`I live in ${houseNumber} ${street} ${city} ${areaCode}`);

// Object Destructuring
const animal = {
    name: "Lolong",
    species: "saltwater crocodile",
    weight: "1075 kgs",
    measurement: "20 ft 3 in",
};

let { name, species, weight, measurement } = animal;
console.log(
    `${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`
);

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];
let resultNumbers = numbers.forEach((number) => {
    console.log(number);
});

// Javascript Classes

class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

let dog1 = new Dog("Frankie", 5, "Miniature Dachshund");
let dog2 = new Dog("Jamies", 2, "Siberian Husky");

console.log(dog1);
console.log(dog2);
